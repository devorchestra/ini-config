let assert = require('assert');
let fs = require('fs');

const filePath = process.env.CONFIG_FILE_PATH;

let configFile;
module.exports = (key, done) => {

    if (configFile) return returnValueByKey(configFile, key, done);

    assert(filePath, 'Config file path is not defined');
    fs.readFile(filePath, 'utf-8', (error, data) => {
        if (error) return done(error);
        try {
            configFile = JSON.parse(data);
        } catch (parseError) {
            return done(parseError);
        }
        returnValueByKey(configFile, key, done);
    });
};

function returnValueByKey(config, key, done) {
    let value = config[key];
    if (!value) return done(new Error(`Key ${key} is not defined in the local file store`));
    process.nextTick(() => {done(null, value)});
}