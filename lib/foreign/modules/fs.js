let fs = require('fs');
let path = require('path');

module.exports = (key, done) => {

    try {
        let value = fs.readFileSync(path.resolve(key));
        if (path.extname(key) === '.json') {
            value = JSON.parse(value);
        }
        done(null, value);

    } catch (error) {
        done(error);
    }
};