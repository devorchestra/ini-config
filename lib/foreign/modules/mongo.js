let MongoClient = require('mongodb').MongoClient;
let async = require('async');
let assert = require('assert');

const HOST = process.env.MONGO_VAULT_HOST;
const PORT = process.env.MONGO_VAULT_PORT || 27017;
const USERNAME = process.env.MONGO_VAULT_USER;
const PASSWORD = process.env.MONGO_VAULT_PASS;
const DB_NAME = process.env.MONGO_VAULT_DB || 'vault';
const COLLECTION_NAME = process.env.MONGO_VAULT_COLLECTION || 'secrets';

module.exports = (key, done) => {
    
    assert(HOST, 'mongo vault host is not defined');
    assert(USERNAME, 'mongo vault username is not defined');
    assert(PASSWORD, 'mongo vault password is not defined');
    
    async.waterfall([
        
        (connectionCreated) => {
            MongoClient.connect(`mongodb://${USERNAME}:${PASSWORD}@${HOST}:${PORT}/${DB_NAME}`, (error, db) => {
                connectionCreated(error, db);
            });
        },

        (db, valueReceived) => {
            let fields = {};
            fields[key] = 1;
            db.collection(COLLECTION_NAME).findOne({}, {fields}, (error, document) => {
                db.close();
                if (error) return valueReceived(error);
                if (!document || !document[key]) return valueReceived(new Error(`value for key ${key} not found`));
                valueReceived(null, document[key]);
            });
        }

    ], done);
};

