let assert = require('assert');
let path = require('path');
let S3 = require('aws-sdk/clients/s3');

const region = process.env.S3_REGION;
const accessKeyId = process.env.S3_ACCESS_KEY_ID;
const secretAccessKey = process.env.S3_SECRET_ACCESS_KEY;
const bucket = process.env.S3_BUCKET;

let s3Client;

module.exports = (key, done) => {

    if (!s3Client) {
        assert(region, 'S3 region is not defined');
        assert(bucket, 'S3 bucket is not defined');
        let awsConfig = {apiVersion: '2006-03-01', region};
        if (accessKeyId) awsConfig.accessKeyId = accessKeyId;
        if (secretAccessKey) awsConfig.secretAccessKey = secretAccessKey;
        s3Client = new S3(awsConfig);
    }

    let s3Params = {
        Bucket: bucket,
        Key: key
    };

    s3Client.getObject(s3Params, (error, data) => {

        if (error) return done(error);

        try {
            let value = data.Body.toString();
            if (path.extname(key) === '.json') {
                value = JSON.parse(value);
            }
            done(null, value);

        } catch (e) {
            return done(e);
        }
    });
};