# INI-config loader

A wrapper for npm [ini module](https://github.com/npm/ini) with support of dynamic variables .

## Usage

    * Add dependency to the package.json : "ini-config": "bitbucket:devorchestra/ini-config"

    * Set NODE_ENV (default is 'development')

    * Configure your Vault

    * See unit tests for examples

#### Dynamic Variables
Currently this module supports following vaults of dynamic variables:

1. Environment of the process.
    Example of usage: ```ENV->VARIABLE_FROM_ENVIRONMENT```


2. Mongodb.
    Example of usage: ```MONGO->VARIABLE_FROM_MONGO_DOCUMENT```. Need to set vault credentials:
    * MONGO_VAULT_HOST
    * MONGO_VAULT_PORT (default is 27017)
    * MONGO_VAULT_USER
    * MONGO_VAULT_PASSWORD
    * MONGO_VAULT_DB (default is 'vault')
    * MONGO_VAULT_COLLECTION (default is 'secrets')


3. AWS S3. Reads content of the specified file as string from S3 bucket and assigns to the variable. If file has extension `.json`, file content will be parsed as js object before assignment. Example of usage: ```S3->dir/file.txt```. BEWARE: file path in bucket start without slash symbol. Need to set vault credentials:
    * S3_REGION
    * S3_ACCESS_KEY_ID (optional)
    * S3_SECRET_ACCESS_KEY (optional)
    * S3_BUCKET

4. Reads all variables from one file from local file system. Example of usage: ```FILE->VARIABLE_FROM_LOCAL_JSON_FILE```. Need to set vault credentials:
    * CONFIG_FILE_PATH

5. Reads content of the specified file as string from the local file system and assigns to the variable. If file has extension `.json`, file content will be parsed as js object before assignment.
Example of usage: ```FS->./test_configs/fs_configs/mongo_profile.json```

## INI config file example

```ini
[default]
port = 4444
host = localhost
[default.log]
level = error

[development]
port = ENV->API_PORT
host = ENV->API_HOST

[development.log]
level = debug

[production]
port = MONGO->API_PORT
host = MONGO->API_HOST

[development.log]
level = info
```

## Unit tests
Run tests by:

`$ npm run test`

## How to use
See index.d.ts types file

## Documentation
You can use `typedoc` to generate documentation of the module:
```
$ typedoc --out docs/ --includeDeclarations index.d.ts
```