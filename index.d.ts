/**
 * load, parse and write config file to object
 *
 * @param {string} filename - path to config.ini file
 * @param {(error: Error, config: any) => void} done
 */
declare function loadConfig(filename: string, done: (error: Error, config: any) => void): void;

export = loadConfig
