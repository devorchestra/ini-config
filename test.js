const assert = require('chai').assert;
const describe = require('mocha').describe;
const it = require('mocha').it;
const configLoader = require('./index');


describe('config loader', () => {

    it('returns basic config', (done) => {
        process.env.NODE_ENV = 'production';
        configLoader('test_configs/basic.ini', (error, config) => {
            assert.equal(config.environment, 'production');
            assert.strictEqual(config.keyOne, 1);
            assert.strictEqual(config.keyTwo, 2);
            assert.strictEqual(config.keyDecimal, 10.5);
            assert.strictEqual(config.array[0], 'one');
            assert.strictEqual(config.array[2], 'three');
            assert.strictEqual(config.array[3], 3);
            assert.equal(config.keyDEV, 'dev');
            assert.equal(config.log.level, 'INFO');
            assert.equal(config.log.time, 100);
            assert.equal(config.composer.log.time, 100);
            assert.isObject(config.empty);
            assert.isTrue(config.bool);
            done();
        })
    });

    it('returns foreign config', (done) => {
        process.env.NODE_ENV = 'development';
        configLoader('test_configs/foreign.ini', (error, config) => {
            assert.equal(config.db.host, 'dummy value');
            assert.equal(config.db.secret, 'secret');
            done();
        })
    });

    it('returns default config if the environment is not described', (done) => {
        configLoader('test_configs/stageOnly.ini', (error, config) => {
            assert.equal(config.secret, 'secret');
            assert.notExists(config.host);
            done();
        })
    });

    it('returns correct config with FS placeholders', (done) => {
        configLoader('test_configs/fs.ini', (error, config) => {
            assert.equal(config.redis.host, 'redishost.com');
            assert.equal(config.mongo.profile.host, 'mongohost.com');
            done();
        })
    });
/*
    it('returns correct config with S3 placeholders', (done) => {

        configLoader('test_configs/s3.ini', (error, config) => {
            assert.equal(config.redis.host, 'redishost.com');
            assert.equal(config.mongo.profile.host, 'mongohost.com');
            done();
        })
    }); */
});
