const fs = require('fs');
const ini = require('ini');
const merge = require('lodash/merge');
const ensureAsync = require('async/ensureAsync');
const traverse = require('traverse-async').traverse;
const processNode = require('./lib/processNode');


module.exports = (filename, done) => {
    const parsed = ini.parse(fs.readFileSync(filename, 'utf-8'));
    const environment = process.env.NODE_ENV || 'development';
    const environmentConfig = parsed[environment];
    const merged = merge(parsed.default, environmentConfig, {environment});
    traverse(merged, ensureAsync(processNode), (config) => {
        done(null, config);
    });
};
